<?php

use yii\db\Migration;

class m170703_085539_create_imgur_tables extends Migration
{
    public function up()
    {

        $this->createTable('api_call_log', [
            'id' => $this->primaryKey(),
            'method' => $this->string(20),
            'url' => $this->text(),
            'created_by' => $this->string(20)->notNull(),
            'created_at'=>$this->timestamp()
        ]);

        $this->createTable('imgur_album', [
            'id' => $this->primaryKey(),
            'imgur_album_id' => $this->string(100),
            'title' => $this->string(100),
            'description' => $this->string(100),
            //'cover' => 'MEDIUMBLOB',
        ]);

        $this->createTable('imgur_image', [
            //'imagehash' => 'MEDIUMBLOB',
            'id' => $this->primaryKey(),
            'title' => $this->string(100),
            'name' => $this->string(100),
            'description' => $this->text(),
            'type' => $this->string(20),
            'album_id' => $this->integer(11)->notNull(),
            'album_hash' => $this->string(21),
            'imgur_image_id' => $this->string(100),
            'url' => $this->string(255),
            'created_at'=>$this->timestamp()
        ]);


        $this->createIndex(
            'idx-imgur-image-album-id',
            'imgur_image',
            'album_id'
        );

        $this->addForeignKey(
            'fk-imgur-image-album-id',
            'imgur_image',
            'album_id',
            'imgur_album',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-imgur-image-album-id', 'imgur_image');
        $this->dropIndex('idx-imgur-image-album-id', 'imgur_image');

        $this->dropTable('api_call_log');
        $this->dropTable('imgur_album');
        $this->dropTable('imgur_image');
    }
}
