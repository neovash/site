<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "imgur_album".
 *
 * @property int $id
 * @property string $imgur_album_id
 *
 * @property ImgurImage[] $imgurImages
 */
class ImgurAlbum extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'imgur_album';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['imgur_album_id'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'imgur_album_id' => 'Imgur Album ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImgurImages()
    {
        return $this->hasMany(ImgurImage::className(), ['album_id' => 'id']);
    }

    

}
