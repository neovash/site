<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;
use yii\imagine\Image;


/**
 * This is the model class for table "imgur_image".
 *
 * @property integer $id
 * @property string $title
 * @property string $name
 * @property string $description
 * @property string $type
 * @property integer $album_id
 * @property string $album_hash
 * @property string $imgur_image_id
 * @property string $url
 * @property string $created_at
 *
 * @property ImgurAlbum $album
 */
class ImgurImage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'imgur_image';
    }

    public $image;
    public $base64Image;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description','image'], 'string'],
         //   [['title',], 'required'],
            ['image', 'file', 'extensions' => ['png', 'jpg'], 'maxSize' => 1024*1024*10,'skipOnEmpty' => true],
            [['album_id'], 'integer'],
            [['created_at','image'], 'safe'],
            [['title', 'name', 'imgur_image_id'], 'string', 'max' => 100],
            [['type'], 'string', 'max' => 20],
            [['album_hash'], 'string', 'max' => 21],
            [['url'], 'string', 'max' => 255],
            [['album_id'], 'exist', 'skipOnError' => true, 'targetClass' => ImgurAlbum::className(), 'targetAttribute' => ['album_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'name' => 'Name',
            'description' => 'Description',
            'type' => 'Type',
            'album_id' => 'Album ID',
            'album_hash' => 'Album Hash',
            'imgur_image_id' => 'Imgur Image ID',
            'url' => 'Url',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlbum()
    {
        return $this->hasOne(ImgurAlbum::className(), ['id' => 'album_id']);
    }

    public function saveToImgur($imageHash = '', $id = null){
        //die($albumHash);

        //create new Album if album_hash is empty
        $album_hash = $this->createAlbum();

        $params = [
            'title' => $this->title,
            'description' => $this->description,
            'image' => $this->base64Image,
            'name' => $this->name,
            'album' => $album_hash //Album ID from imgur
        ];

        
        $imgur = new ImgurApi;

        $response = $imgur->connect(
              ($imageHash == '')?  'post' : 'put', //method 
              'image/'. $imageHash, //url
              $params
              );

        if($imageHash == '')
            return $response->data;
        else
            return $response->data;
        
    }

    private function createAlbum(){
        if($this->album_hash == '')
        {
            $album = new \app\models\ImgurAlbum;
            $album->title  = $this->title;
            $album->description = $this->description;

            $album->save();
            $this->album_id = $album->id;
            return $album->imgur_album_id;
        } else {
            return $this->album_hash;
        }

    }

    private function imageToBase64($file){
        if($file)
        {   
            
            if ($file->size !== 0){
                    $tempFile = file_get_contents($file->tempName);
                    $this->base64Image = base64_encode($tempFile);
            }

        }
    }

    

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            $this->imageToBase64(UploadedFile::getInstance($this,'image'));

            if($this->isNewRecord){
                $data = $this->saveToImgur();
                $this->imgur_image_id = $data->id;
                $this->type = $data->type; 
                $this->url =  $data->link;
            }else{
                $this->saveToImgur($this->imgur_image_id);
            }
            


            return true;
        } else {
            return false;
        }
    }

}
