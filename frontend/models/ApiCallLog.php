<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "api_call_log".
 *
 * @property integer $id
 * @property string $method
 * @property string $url
 * @property string $created_by
 * @property string $created_at
 */
class ApiCallLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'api_call_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url'], 'string'],
            [['created_by'], 'required'],
            [['created_at'], 'safe'],
            [['method', 'created_by'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'method' => 'Method',
            'url' => 'Url',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
        ];
    }
}
