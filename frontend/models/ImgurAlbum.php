<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "imgur_album".
 *
 * @property integer $id
 * @property string $imgur_album_id
 * @property string $title
 * @property string $description
 *
 * @property ImgurImage[] $imgurImages
 */
class ImgurAlbum extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'imgur_album';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['imgur_album_id', 'title', 'description'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'imgur_album_id' => 'Imgur Album ID',
            'title' => 'Title',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImgurImages()
    {
        return $this->hasMany(ImgurImage::className(), ['album_id' => 'id']);
    }


    //get my hndev2 album list
    public function getImgurAlbums()
    {
        $imgur = new ImgurApi;
        return $imgur->connect('get', 'account/hndev2/albums/');
    }

    public function saveToImgur($albumHash = '', $id = null){
        //die($albumHash);
        $params = [
            'id' => $id, //imageId, if any
            'title' => $this->title,
            'description' => $this->description
        ];


        $imgur = new ImgurApi;

        $response =  $imgur->connect(
              ($albumHash == '')?  'post' : 'put', //method 
              'album/'. $albumHash, //url
              $params
        );

        if($albumHash == '')
            return $response->data->id;
        else
            return $response->data;
        
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if($this->isNewRecord){
                $this->imgur_album_id = $this->saveToImgur();
            }else{
                $this->saveToImgur($this->imgur_album_id);
            }
            

            return true;
        } else {
            return false;
        }
    }


}
