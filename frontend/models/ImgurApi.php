<?php

namespace app\models;


use linslin\yii2\curl\Curl;
use app\models\ApiCallLog;

Class ImgurApi
{

	public function connect($method, $url, $params = [])
	{
		$curl = new Curl();

		$imgur = 'https://api.imgur.com/3/'.  $url;

		$curl->setOption(CURLOPT_HTTPHEADER,['Authorization: Bearer 33e09b847aa0e3999af2cb2434f2333a14bebcf4'])
		->setOption(CURLOPT_SSL_VERIFYPEER, false); //this is effin  line that  took 80% of my time. lol

		// GET Method
		if($method == 'get'){
			if($params != Null)
				$curl->setGetParams($params);
			$response = $curl->get($imgur);
		} else {
			if($params != Null)
			$curl->setPostParams($params);
			$response = $curl->$method($imgur);
		}

		$response = json_decode($response);
		return $response; 

	} 


	//save API Logs
	public function saveLog($url, $method, $created_by = 'hndev2')
	{
		$log  = new ApiCallLog;
		$log->url = $url;
		$log->method = $method;
		$log->created_by = $created_by;
		$log->created_at = date('Y-m-d H:i:s');
		$log->save();
	}


}