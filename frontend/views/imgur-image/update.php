<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ImgurImage */

$this->title = 'Update Imgur Image: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Imgur Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="imgur-image-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
