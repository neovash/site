<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ImgurImage */

$this->title = 'Create Imgur Image';
$this->params['breadcrumbs'][] = ['label' => 'Imgur Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="imgur-image-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
