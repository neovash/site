<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ImgurAlbum */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Imgur Albums', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="imgur-album-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Add Image', ['/imgur-image/create-by-album', 'id' => $_GET['id']], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'imgur_album_id',
            'title',
            'description',
        ],
    ]) ?>

</div>
