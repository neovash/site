<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ImgurAlbum */

$this->title = 'Update Imgur Album: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Imgur Albums', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="imgur-album-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
